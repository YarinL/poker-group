#pragma once

enum  Rank : int { Ace = 1, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King };
enum  Suit : int { Spades = 1, Hearts, Diamonds, Clubs };

class CCard 
{
public:
	CCard( );
	CCard( unsigned int &suit, unsigned int &rank );
	~CCard( );
	

	int GetSuit() { return m_suit; }
	int GetRank() { return m_rank; }
	string GetCardString();



private:
	int m_suit;
	int m_rank;
};

