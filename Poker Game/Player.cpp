#include "stdafx.h"
#include "Player.h"




CPlayer::CPlayer( string Name, unsigned int id ) : m_player_name( Name ), m_player_id( id )
{
	m_dealer = false;
	m_small_blind = false;
	m_big_blind = false;
	m_fold = false;
	m_last_bet = 0;
	m_money = 0;
	m_status = 0;
}

CPlayer::~CPlayer( )
{
}

void CPlayer::SetStatus( int status )
{
	switch( status )
	{
		case Dealer: m_status = 1; break;
		case SmallBlind: m_status = 2; break;
		case BigBlind: m_status = 3; break;
		case Folded: m_status = 4; m_fold = true; break;
		default: m_status = 0; break;
	}
}

void CPlayer::PlaceBet( unsigned int money )
{
	m_money -= money;
	m_last_bet += money;
}

void CPlayer::SetRole( unsigned int role )
{
	switch( role )
	{
		case 1:	m_dealer = true; break;
		case 2:	m_small_blind = true; break;
		case 3:	m_big_blind = true; break;
		default: throw std::logic_error( "Invalid position." );
	}
}

void CPlayer::DrawCard( CPlayer& Player )
{
	if( m_dealer )
	{
		CDeck Deck;
		CCard Card = Deck.Draw( );
		Player.HoleCards.push_back( Card );
		return;
	}
	throw std::logic_error( "Player is not a dealer." );
	
}

void CPlayer::DrawCards( CPlayer& Player, unsigned int card_number )
{
	if(m_dealer )
	{
		CDeck Deck;
		for( unsigned int i = 0; i < card_number; i++ )
		{
			CCard Card = Deck.Draw();
			Player.HoleCards.push_back( Card );
		}
		return;
	}
	 throw std::logic_error( "Invalid position." );
}

void CPlayer::ViewCards( )
{
	if( HoleCards.size() > 1 )
	{
		for( auto& card : HoleCards )
		{
			printf( "Cards in your hand:\n" );
			printf("- %s %s\n",card.GetSuit(),card.GetRank());
		}
	}
}

void CPlayer::SetMoney( unsigned int money )
{
	m_money = money;
}