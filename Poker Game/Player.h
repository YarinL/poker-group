#pragma once

class CPlayer
{
public:
	CPlayer( );
	CPlayer( string Name, unsigned int id );
	~CPlayer( );

	
	void SetStatus( int status );
	void PlaceBet( unsigned int money );
	void SetRole( unsigned int position );
	void DrawCard( CPlayer& Player );
	void DrawCards( CPlayer& Player, unsigned int card_number );
	void SetMoney( unsigned int money );


	string GetPlayerName( ) { return m_player_name; }
	unsigned int GetPlayerId( ) { return m_player_id; }
	unsigned int GetMoney( ) { return m_money; }
	unsigned int GetLastBet() { return m_last_bet; }
	unsigned int GetStatus( ) { return m_status; }

	void ViewCards();
	


private:
	int m_player_id;
	string m_player_name;
	int m_status;
	bool m_dealer;
	bool m_small_blind;
	bool m_big_blind;
	bool m_fold;

	unsigned int m_last_bet;
	unsigned int m_money;

	vector<CCard> HoleCards;
};

