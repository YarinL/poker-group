#include "stdafx.h"
#include "Table.h"



CTable::CTable( unsigned int Number, string Name, unsigned int Money ) : m_table_number( Number ), m_table_name( Name )
{
	SetMoneyCount( Money );
	m_current_bet = 0;
	m_dealer = nullptr;
	m_small_blind = nullptr;
	m_big_blind = nullptr;
}

CTable::~CTable( )
{
}

void CTable::SetMoneyCount( unsigned int Money )
{
	m_pot = Money;
}


void CTable::AddPlayer( CPlayer& Player )
{
	if( m_players_count <= MAX_PLAYERS )
	{
		Players.insert(pair<int,CPlayer>(m_players_count+1, Player) );
		m_players_count++;
	}
	else
		throw std::logic_error( "Table is full." );
	
}


CPlayer CTable::GetPlayer( unsigned int player_number )
{
	if(player_number <= GetPlayerCount() )
		for( auto& player : Players )
		{
			if( player.first == player_number )
				return player.second;
		}
		
	throw std::logic_error( "Error: Player is not exist." );
}

unsigned int CTable::GetPlayerPos( unsigned int player_id )
{
	for( auto& player : Players )
	{
		if( player.second.GetPlayerId == player_id )
			return player.first;
	}

	throw std::logic_error( "Error: Player is not exist." );
}

void CTable::SetDealer( CPlayer& Player )
{
	Player.SetRole( Dealer );
	m_dealer = &Player;
	SetCurrentPlayer( Player );

}

void CTable::SetSmallBlind( CPlayer& Player )
{
	Player.SetRole( SmallBlind );
	m_small_blind = &Player;
	SetCurrentPlayer( Player );

	printf( "Place small blind Bet: " );
	unsigned int bet;
	scanf( "%d", bet );
	SetBet( Player, bet );
}

void CTable::SetBigBlind( CPlayer& Player )
{
	Player.SetRole( BigBlind );
	m_big_blind = &Player;
	SetCurrentPlayer( Player );

	printf( "Place big blind Bet: " );
	unsigned int bet;
	scanf( "%d", bet );
	SetBet( Player, bet );
}

void CTable::SetCurrentPlayer( CPlayer& Player )
{
	m_current_player = &Player;
}

void CTable::SetNextPlayer()
{
	m_current_player = GetNextPlayer();
	printf( "Current Player is: '%s'", GetCurrentPlayer() );
}

CPlayer* CTable::GetNextPlayer( )
{
	int i = GetPlayerPos(GetCurrentPlayer()->GetPlayerId());
	if( i <= GetPlayerCount() )
	{
		if( Players[++i].GetStatus != Folded )
		{
			m_next_player = &GetPlayer( i );
			printf( "~ Next Player is: '%s'\n", m_next_player );
			return m_next_player;
		}

		m_next_player = &GetPlayer( ++i );
		printf( "~ Next Player is: '%s'\n", m_next_player );
		return m_next_player;
	}	
	else
	{
		i = 1;
		m_next_player = &GetPlayer( i );
		printf( "~ Next Player is: '%s'\n", m_next_player );
		return m_next_player;
	}
		
}

void CTable::SetBet( CPlayer& Player, unsigned int amount )
{
	while( 1 )
	{
		if( amount > m_current_bet + 10 )
		{
			if( amount <= Player.GetMoney( ) )
				Player.PlaceBet( amount );
			else
			{
				printf( "Player '%s' don't have the amount of cash!", Player.GetPlayerName( ) );
				Player.SetStatus( Folded );
				return;
			}

			if( Player.GetStatus( ) == SmallBlind )
				m_sb = amount;
			else if( Player.GetStatus() == BigBlind )
				m_bb = amount;

			m_current_bet = amount;

			printf( "Player '%s' betted %d amount.\n", Player.GetPlayerName( ), amount );
			break;
		}
		else
			printf( "Place a higher bet!" );
	}
}
