#include "stdafx.h"
#include "Util.h"


CUtil::CUtil()
{
}


CUtil::~CUtil()
{
}

bool CUtil::IsValidCharacter( char cb )
{

	if( ( cb >= 'a' && cb <= 'z' ) ||
		( cb >= 'A' && cb <= 'Z' ) ||
		( cb >= '0' && cb <= '9' ) )
		return true;

	switch( cb )
	{
		case ' ':
		case ',':
		case '.':
		case ':':
		case ';':
		case '!':
		case '/':
		case '?':
		case '\\':
		case '@':
		case '"':
		case '\'':
		case '%':
		case '$':
		case '#':
		case '{':
		case '}':
		case '[':
		case ']':
		case '(':
		case ')':
		case '+':
		case '-':
		case '*':
		case '=':
		case '^':
		case '_':
		case '~':
			return true;
	}

	return false;

}

void CUtil::Input( char* CurrentString )
{
	char * pCurrentString = CurrentString;

	while( 1 )
	{
		if( _kbhit( ) )
		{
			int op = 0;
			int ch = _getch( );
			if( ch == 0 ||
				ch == 0xe0 )
			{
				op = ch;
				ch = _getch( );
			}
			if( IsValidCharacter( ch ) )
			{
				*pCurrentString++ = ( char )ch;
				printf( "%c", ch );
			}

			else if( ch == VK_BACK ) // Backspace
			{
				if( pCurrentString > CurrentString )
				{
					*( --pCurrentString ) = 0;
					printf( "\b \b" );
				}

			}
			else if( ch == VK_RETURN ) // Enter
			if( pCurrentString > CurrentString )
			{
				*pCurrentString = 0;
				printf( "\n" );
				break;
			}
		}
	}
}