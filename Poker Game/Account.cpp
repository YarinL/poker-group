#include "Account.h"


CAccount::CAccount(int accountId, const string& accountName, const tm& regDate)
{
	m_accountId = accountId;
	m_accountName = accountName;
	m_rank        = Regulars;

	//TODO: define account starting money for now 20k...
	m_money = 0;
	setMoney(20000);

	m_maxPotWon   = 0;
	m_bestHand = NoHand;
	m_handsPlayed = 0;
	m_handsWon    = 0;

	m_regDate = regDate;
}


CAccount::~CAccount()
{

}

void CAccount::addNewGameToHistory(const CGameStats& newGame)
{
	// update the Over all stats for the account;
	if (m_maxPotWon < newGame.m_maxPotWon)
		m_maxPotWon = m_maxPotWon;

	if (m_bestHand < newGame.m_bestHand)
		m_bestHand = newGame.m_bestHand;

	m_handsPlayed += newGame.m_handsPlayed;
	m_handsWon += newGame.m_handsWon;

	m_gameHistory.push_back(newGame);
}

void CAccount::viewGameHistory()
{
	for (int i = 0; i < m_gameHistory.size(); i++)
	{
		tm& curgameDate = m_gameHistory[i].m_gameDate;
		printf("Game Date    : %d/%d/%d \n\n", curgameDate.tm_mday, curgameDate.tm_mon, curgameDate.tm_year);
		printf("Table %d", m_gameHistory[i].m_tableNum);
		printf("Game Stats   : \n");
		printf("Best hand    : %s \n", handTypeStrings[m_gameHistory[i].m_bestHand]);
		printf("Hands played : %d \n", m_gameHistory[i].m_handsPlayed);
		printf("Hands Won    : %d \n", m_gameHistory[i].m_handsWon);
		printf("Max Pot Won  : %lu \n", m_gameHistory[i].m_maxPotWon);
		printf("Money gained : %lu \n", m_gameHistory[i].m_moneyGained);
		printf("Money Lost   : %lu \n\n", m_gameHistory[i].m_moneyLost);
		
	}
}

void CAccount::viewAccountStats()
{
	printf("Account Info : \n");
	printf("Account ID   : %d \n", m_accountId);
	printf("Account Name : %s \n", m_accountName);
	printf("Account Rank : %s \n", playerRanksStrings[m_rank]);
	printf("Registration Date : %d/%d/%d \n",m_regDate.tm_mday,m_regDate.tm_mon,m_regDate.tm_year);

	printf("Account Stats: \n");
	printf("Account Money : %d \n", m_money);
	printf("Best Hand     : %s \n", handTypeStrings[m_bestHand]);
	printf("Hands played  : %d \n", m_handsPlayed);
	printf("Hands Won     : %d \n", m_handsWon);
	printf("Max Pot Won   : %lu \n\n", m_maxPotWon);

}

void CAccount::setMoney(unsigned long money)
{
	m_money += money;

	// setting rank according to current money amount
	if (m_money >= 50000 && m_money <= 100000)
		m_rank = Regulars;

	if (m_money >= 110000 && m_money < 500000)
		m_rank = Permium;

	if (m_money >= 500000)
		m_rank = Special;
}

int CAccount::getAccountId()
{
	return m_accountId;
}

string CAccount::getAccountName()
{
	return m_accountName;
}

PlayerRank CAccount::getAccountRank()
{
	return m_rank;
}

unsigned long CAccount::getMoney()
{
	return m_money;
}

unsigned long CAccount::getMaxPotWon()
{
	return m_maxPotWon;
}

unsigned int CAccount::getHandsPlayed()
{
	return m_handsPlayed;
}

unsigned int CAccount::getHandsWon()
{
	return m_handsWon;
}

tm CAccount::getRegDate()
{
	return m_regDate;
}
