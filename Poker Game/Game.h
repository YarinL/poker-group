#pragma once



class CGame
{
public:
	CGame(CTable& Table);
	~CGame();
	 
	void GameInit();  // Set the game details, roles and so.
	void GameLoop();

	void InitRoles();
	void Call( CPlayer& Player );
	void Raise( CPlayer& Player );
	void Fold( CPlayer& Player );
	void Check( CPlayer& Player );

	 
private:
	CTable m_table;
	unsigned int m_index;



};

