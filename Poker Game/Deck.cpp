#include "stdafx.h"
#include "Deck.h"


CDeck::CDeck( )
{
	for( unsigned int i = 1; i < MAX_SUIT; i++ )
		for( unsigned int j = 1; i < MAX_RANK; i++ )
		{
			CCard card( i, j );
			m_deck.push_back( card );
		}
}


CDeck::~CDeck( )
{
}

CCard CDeck::Draw( )
{
	Shuffle();
	CCard card = m_deck.back();
	m_deck.pop_back();	    
	return card;
}

void CDeck::Shuffle( )
{
	std::random_shuffle( m_deck.begin( ), m_deck.end( ) );
}