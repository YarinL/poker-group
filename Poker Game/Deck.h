#pragma once

const auto MAX_DECK = 52;
const auto MAX_RANK = 13;
const auto MAX_SUIT = 4;

class CDeck
{
public:
	CDeck( );
	~CDeck( );

	CCard Draw();
	void Shuffle();

private:
	vector<CCard> m_deck;
};

