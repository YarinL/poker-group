#pragma once

#include "stdafx.h"
#include <vector>
#include <ctime>

enum HandType   : int { NoHand, HighCard, OnePair, TwoPair, ThreeOfAKind, Straight, Flush, FullHouse, FourOfAKind, StraightFlush,
RoyalFlush};

// used to easily convert the handType enum to strings
const char* handTypeStrings[11] = { "None", "High Card", "One Pair", "Two Pair", "Three of a Kind", "Straight", "Flush"
, "Full House", "Four Of A Kind", "Straight Flush", "Royal Flush" };

// used to easily convert the PlayerRank enum to strings
enum PlayerRank : int { Regulars, Permium, Special};
const char* playerRanksStrings[3] = { "Regulars", "Permium", "Special" };


struct CGameStats
{
	tm m_gameDate;
	
	unsigned int  m_tableNum;

	//Game stats for a single Game
	HandType	  m_bestHand;
	unsigned int  m_handsPlayed;
	unsigned int  m_handsWon;
	unsigned long m_maxPotWon;
	unsigned long m_moneyGained;
	unsigned long m_moneyLost;
};

class CAccount
{
public:
			 CAccount(int accountId, const string& accountName, const tm& regDate);
	virtual ~CAccount();

	void		  addNewGameToHistory(const CGameStats& newGame);
	void		  viewGameHistory	 ();
	void		  viewAccountStats	 ();

	void		  setMoney			 (unsigned long money);

	int			  getAccountId		 ();
	string		  getAccountName	 ();
	PlayerRank	  getAccountRank	 ();

	unsigned long getMoney			 ();
	unsigned long getMaxPotWon		 ();
	unsigned int  getHandsPlayed	 ();
	unsigned int  getHandsWon		 ();

	tm			  getRegDate		 ();

private:
	int				   m_accountId;
	string			   m_accountName;
	PlayerRank		   m_rank;

	unsigned long	   m_money;

	//Over all Games stats for the Account
	unsigned long	   m_maxPotWon;
	HandType		   m_bestHand;
	unsigned int	   m_handsPlayed;
	unsigned int	   m_handsWon;

	tm				   m_regDate;

	vector<CGameStats> m_gameHistory;

};

