#pragma once

enum Position { None, Dealer = 1, SmallBlind, BigBlind, Folded };

struct CDealer
{
public:
	CDealer( CPlayer player );
						
	
};

const auto MAX_PLAYERS = 10;


class CTable
{
public:
	CTable( );
	CTable( unsigned int Number, string Name, unsigned int Money );
	~CTable( );

	void SetMoneyCount( unsigned int Money );
	void AddPlayer( CPlayer& Player );
	void SetCurrentPlayer( CPlayer& Player );
	void SetNextPlayer();
	void SetCards( CPlayer& Player );
	void SetBet( CPlayer& Player, unsigned int amount );
	void SetDealer( CPlayer& Player );
	void SetSmallBlind( CPlayer& Player );
	void SetBigBlind( CPlayer& Player );


	CPlayer GetPlayer( unsigned int player_number );
	unsigned int GetPlayerPos( unsigned int player_id );
	unsigned int GetPlayerCount( ) { return Players.size( ); }

	unsigned int GetTableCount( ) { return m_table_number; }
	string GetTableName( ) { return m_table_name; }

	unsigned int GetBet( ) { return m_pot; }

	unsigned int GetSmallBlind( ) { return m_sb; }
	unsigned int GetBigBlind( ) { return m_bb; }

	CPlayer* GetDealer() { return m_dealer; }
	CPlayer* GetSmallBlindPlayer() { return m_small_blind; }
	CPlayer* GetBigBlindPlayer() { return m_big_blind; }

	CPlayer* GetCurrentPlayer( ) { return m_current_player; }
	CPlayer* GetNextPlayer( );

private:
	unsigned int m_table_number;
	string m_table_name;
	unsigned int m_players_count;
	unsigned int m_current_bet;
	unsigned int m_pot;
	unsigned int m_sb;
	unsigned int m_bb;

	CPlayer* m_dealer;
	CPlayer* m_small_blind;
	CPlayer* m_big_blind;

	CPlayer* m_current_player;
	CPlayer* m_next_player;

	std::map<int, CPlayer> Players;
};

