#include "stdafx.h"

namespace
{
	struct Internal_s
	{
		unsigned int table_count = 0;
		unsigned int player_count = 0;

		vector<CTable> Tables;
		vector<CPlayer> Players;

	} *_internal;
}

void SetTable( string name, unsigned int money )
{
	CTable Table( ++_internal->table_count, name, money );
	_internal->Tables.push_back( Table );
}

CPlayer CreatePlayer( string Name )
{
	CPlayer Player( Name, ( _internal->player_count )++ );
	_internal->Players.push_back( Player );
	printf( "-Player '%s' has been created!\n", Name.c_str() );
	return 	Player;
}

int main()
{

	CUtil Util;
	char szCurrentString[ 256 ];

	_internal = new Internal_s();


	printf( "Poker Game!\n" );
	printf( "\n" );

	printf( "Input your name: " );
	Util.Input( szCurrentString );

	CPlayer MainPlayer = CreatePlayer( szCurrentString );

	printf( "\n" );

	while( 1 )
	{
		printf( "Input command: " );
		Util.Input( szCurrentString );

		//Create Table
		if( strcmp( szCurrentString, "/NewTable" ) == 0)
		{
			char TableName[50];
			printf( "Enter table's name: " );
			Util.Input( TableName );

			SetTable( TableName, 800 );
			printf( "Table(id=%d) '%s' has been created!\n", _internal->table_count, _internal->Tables.front( ).GetTableName( ).c_str( ) );
			
			_internal->Tables.front().AddPlayer( MainPlayer );

			//Add Bot Players
			if( _internal->Players.size() < 2 )
			{
				int PlayerNum;
				printf( "Select number of players (10 maximum): " );
				scanf_s( "%d", &PlayerNum );

				if( PlayerNum > 2 || PlayerNum < 10 )
				{	
					for( int i = 1; i <= PlayerNum; i++ )
					{
						ostringstream str;
						str << "Player " << i;
						CPlayer BotPlayer = CreatePlayer( str.str() );
						BotPlayer.SetMoney( 800 );
						_internal->Tables.front().AddPlayer( BotPlayer );
					}
				}		
			}
			
			while( 1 )
			{
				// Set players role
				int i = 0;
				int dealer = ( _internal->Tables.front( ).GetPlayer( i ).GetPlayerId( ) ) % _internal->Tables.front( ).GetPlayerCount( );
				_internal->Tables.front( ).InitRoles( dealer );

				if( i < _internal->Tables.front( ).GetPlayerCount( ) )
					i++;
				else
					i = 1;


			}
		}
	/*	if( strcmp( szCurrentString, "/NewPlayer" ) == 0 )
		{
			char PlayerName[ 50 ];
			printf( "Enter Player's name: " );
			scanf_s( "%s", &PlayerName );

			_internal->Players.push_back( CreatePlayer( PlayerName ) );
			
		}*/
	}

	delete (_internal);	
}