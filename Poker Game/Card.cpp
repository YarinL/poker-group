#include "stdafx.h"
#include "Card.h"

CCard::CCard( unsigned int &suit, unsigned int &rank ) : m_suit( suit ), m_rank( rank )
{

}

CCard::~CCard( )
{
}

string CCard::GetCardString( )
{
	string Suit, Rank;

	switch( m_suit )
	{
		case Spades:	Suit = "Spades"; break;
		case Hearts:	Suit = "Hearts"; break;
		case Diamonds:	Suit = "Diamonds"; break;
		case Clubs:		Suit = "Clubs"; break;
		default: throw std::logic_error( "invalid Suit" );
	}
	switch( m_rank )
	{
		case Ace:	Suit = "Ace"; break;
		case Two:	Suit = "Two"; break;
		case Three:	Suit = "Three"; break;
		case Four:		Suit = "Four"; break;
		case Five:		Suit = "Five"; break;
		case Six:		Suit = "Six"; break;
		case Seven:		Suit = "Seven"; break;
		case Nine:		Suit = "Nine"; break;
		case Ten:		Suit = "Ten"; break;
		case Jack:		Suit = "Jack"; break;
		case Queen:		Suit = "Queen"; break;
		case King:		Suit = "King"; break;
		default: throw std::logic_error( "invalid Suit" );
	}

	return Suit + Rank;
}
