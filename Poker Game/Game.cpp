#include "stdafx.h"
#include "Game.h"


CGame::CGame( CTable& Table )
{
	m_index = 1;
}

CGame::~CGame()
{
}

void CGame::InitRoles( )
{
	if( m_table.GetDealer( ) == nullptr || m_table.GetSmallBlindPlayer( ) == nullptr || m_table.GetBigBlindPlayer( ) == nullptr )
	{
		printf( "~ Player '%s' set as Dealer.\n", m_table.GetCurrentPlayer() );
		m_table.SetDealer(  *m_table.GetCurrentPlayer() );
		m_table.SetNextPlayer();

		printf( "~ Player '%s' set as SmallBlind.\n", m_table.GetCurrentPlayer( ) );
		m_table.SetSmallBlind( *m_table.GetCurrentPlayer( ) );
		m_table.SetNextPlayer( );

		printf( "~ Player '%s' set as BigBlind.\n", m_table.GetCurrentPlayer( ) );
		m_table.SetBigBlind( *m_table.GetCurrentPlayer( ) );
		m_table.SetNextPlayer( );
	}
	else
	{
		//int dealer = ( m_table.GetPlayer( m_index ).GetPlayerId( ) ) % m_table.GetPlayerCount( );
		//unsigned int dealer = m_table.GetPlayerPos( m_table.GetDealer( )->GetPlayerId ) + 1;
		//unsigned int smallblind = m_table.GetPlayerPos( m_table.GetSmallBlindPlayer()->GetPlayerId ) + 1;
		//unsigned int bigblind = m_table.GetPlayerPos( m_table.GetBigBlindPlayer->GetPlayerId ) + 1;

		m_table.SetCurrentPlayer( *m_table.GetDealer() );
		m_table.SetNextPlayer();


		m_table.SetDealer(  *m_table.GetCurrentPlayer() );
		printf( "~ Player '%s' set as Dealer.\n", m_table.GetDealer() );
		m_table.SetNextPlayer( );

		m_table.SetSmallBlind( *m_table.GetCurrentPlayer( ) );
		printf( "~ Player '%s' set as Dealer.\n", m_table.GetSmallBlindPlayer );
		m_table.SetNextPlayer( );

		m_table.SetBigBlind( *m_table.GetCurrentPlayer( ) );
		printf( "~ Player '%s' set as Dealer.\n", m_table.GetBigBlindPlayer );
		m_table.SetNextPlayer( );
	}
}

void CGame::Call( CPlayer& Player )
{
	if( Player.GetLastBet() < m_table.GetBigBlind() )
		m_table.SetBet( Player, m_table.GetBigBlind - Player.GetLastBet() );

}
void CGame::Raise( CPlayer& Player )
{
	printf( "Raise amount: " );
	unsigned int bet;
	scanf( "%d", bet );
	m_table.SetBet( Player, bet );
}

void CGame::Fold( CPlayer& Player )
{
	Player.SetStatus( Folded );
}

void CGame::Check( CPlayer& Player )
{
	if( Player.GetLastBet() >= m_table.GetBigBlind() )
		return;
	else
		printf( "You must call or raise." );
}

void CGame::GameInit( )
{
	// Set Roles
	InitRoles();

	m_table.SetCurrentPlayer( *m_table.GetDealer() );
	m_table.SetNextPlayer();

	for( auto& player = *m_table.GetCurrentPlayer(); player.GetPlayerId() != m_table.GetDealer()->GetPlayerId(); m_table.SetNextPlayer() )
		m_table.GetDealer()->DrawCards( player, 2 );
	m_table.GetDealer( )->DrawCards( *m_table.GetDealer( ), 2 );
	
}

void CGame::GameLoop( )
{
	while( 1 )
	{
		GameInit( );
		for( int i = 0; i < 5; i++ )
		{
				
		}
		
	}
}